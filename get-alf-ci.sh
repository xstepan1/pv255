#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: get-alf-ci <username> <password>" 1>&2
    exit 1
fi

if [ -f /root/.local/share/unity3d/Unity/Unity_lic.ulf ]; then
  rm /root/.local/share/unity3d/Unity/Unity_lic.ulf
fi

xvfb-run -a \
/opt/Unity/Editor/Unity \
    -logFile /dev/stdout \
    -batchmode \
    -quit \
    -username "$1" -password "$2" |
        tee ./unity-output.log

cat ./unity-output.log |
  grep 'LICENSE SYSTEM .* Posting *' |
  sed 's/.*Posting *//' > ./unity3d.alf

rm ./unity-output.log

echo "Go to 'https://license.unity3d.com/manual'"
