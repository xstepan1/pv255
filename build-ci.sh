#!/bin/bash

UNITY_SHARE=/root/.local/share/unity3d/Unity

if [ ! -d $UNITY_SHARE ]; then
    mkdir -p $UNITY_SHARE
fi

cp $1 $UNITY_SHARE/Unity_lic.ulf

if [ ! -d ./Builds ]; then
    mkdir ./Builds
fi

unity-editor \
    -logFile /dev/stdout \
    -batchmode \
    -projectPath $(pwd) \
    -quit \
    -buildTarget WebGL \
    -customBuildPath $(pwd)/Builds \
    -executeMethod Build.Perform
