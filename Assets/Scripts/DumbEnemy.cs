﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumbEnemy : MonoBehaviour, IKillable
{
    public GameObject Player;
    public int StartHealth = 100;
    public float Speed = 3;
    public ScoreKeeper ScoreKeeper;

    private new Rigidbody rigidbody;
    private CenterBar bar;

    public int Health { get; set; }

    private Quaternion originalRotation;

    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(0, 0, -3);
        bar = GetComponentInChildren<CenterBar>();
        Health = StartHealth;
        originalRotation = rigidbody.rotation;
    }

    public void Update()
    {
        if (Health <= 0)
        {
            if (!EndCondition.IsGameOver)
            {
                ScoreKeeper.Score++;
            }
            gameObject.Explode();
            Destroy(gameObject);
        }
        bar.SetValue((float)Health / StartHealth);
    }

    public void FixedUpdate()
    {
        var direction = EndCondition.IsGameOver ? Vector3.back : Player.transform.position - transform.position;
        rigidbody.velocity = (Vector3.back * 2 + direction.normalized).normalized * Speed;
        rigidbody.rotation = originalRotation * Quaternion.FromToRotation(Vector3.back, rigidbody.velocity);
    }
}
