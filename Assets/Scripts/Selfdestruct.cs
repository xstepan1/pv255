﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selfdestruct : MonoBehaviour
{
    public Rect Bounds;

    void Update()
    {
        if (transform.position.x > Bounds.xMax
            || transform.position.x < Bounds.xMin
            || transform.position.z > Bounds.yMax
            || transform.position.z < Bounds.yMin)
        {
            gameObject.Explode();
            Destroy(gameObject);
        }
    }
}
