﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatRequest : MonoBehaviour
{
    public AudioClip Clip;
    public float Volume = 1;
    public bool Loop = true;

    public void Awake()
    {
        BeatMaster.Instance.Play(Clip, Volume, true, Loop);
    }
}
