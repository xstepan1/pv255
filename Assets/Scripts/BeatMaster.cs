﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatMaster : MonoBehaviour
{
    public static BeatMaster Instance = null;

    private AudioSource source;

    public float Time;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        source = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    public void Update()
    {
        Time = source.time;
    }

    public void Play(
        AudioClip clip,
        float volume,
        bool ignoreAlreadyPlaying = true,
        bool loop = true,
        Action onEnd = null)
    {
        if (source.clip == clip && ignoreAlreadyPlaying)
        {
            return;
        }

        source.loop = loop;
        source.clip = clip;
        source.volume = volume;
        source.Play();

        if (onEnd is object)
        {
            StartCoroutine(EndCoroutine(onEnd));
        }
    }

    public void PlayOnce(AudioClip clip, float volume = 1)
    {
        if (source is object && clip != null)
        {
            source.PlayOneShot(clip, volume);
        }
    }

    private IEnumerator EndCoroutine(Action onEnd)
    {
        yield return new WaitForSeconds(source.clip.length - source.time);
        onEnd();
    }
}
