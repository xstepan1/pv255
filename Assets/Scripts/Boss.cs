﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour, IKillable
{
    public Vector3 AttackPosition;
    public float InitialMoveLength = 5;
    public int StartHealth = 1000;
    public CenterBar Bar;

    private float initialTime = 0;
    private Vector3 initialPosition;

    public int Health { get; set; }

    public void Start()
    {
        initialPosition = transform.position;
        initialTime = Time.fixedTime;
        Health = StartHealth;
    }

    public void Initialize(GameObject player, ScoreKeeper scoreKeeper)
    {
        foreach(var enemy in GetComponentsInChildren<AuxilaryEnemy>())
        {
            enemy.Player = player;
            enemy.ScoreKeeper = scoreKeeper;
        }
    }

    public void FixedUpdate()
    {
        gameObject.transform.position = Vector3.Lerp(
            initialPosition,
            AttackPosition,
            (Time.fixedTime - initialTime) / InitialMoveLength);
    }

    public void Update()
    {
        if (Health <= 0)
        {
            if (!EndCondition.IsGameOver)
            {
                EndCondition.IsGameWon = true;
            }
            gameObject.Explode();
        }
        Bar.SetValue((float)Health / StartHealth);
    }
}
