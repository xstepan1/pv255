﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreKeeper : MonoBehaviour
{
    public const string ScorePrefsKey = "Score";

    public TextMeshProUGUI Text;
    public int Padding = 9;

    public int Score { get; set; }

    public void Awake()
    {
        Score = PlayerPrefs.GetInt(ScorePrefsKey, 0);
    }

    public void Update()
    {
        if (Text is object)
        {
            Text.text = Score.ToString().PadLeft(9, '0');
        }
    }

    public void OnDestroy()
    {
        PlayerPrefs.SetInt(ScorePrefsKey, Score);
        PlayerPrefs.Save();
    }
}
