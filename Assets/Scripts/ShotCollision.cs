﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCollision : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        gameObject.Explode();
        Destroy(gameObject);
    }
}
