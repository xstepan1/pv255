﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterBar : MonoBehaviour
{
    public RectTransform Fill;

    public void SetValue(float value)
    {
        var minX = (1 - value) / 2;
        Fill.anchorMin = new Vector2(minX, Fill.anchorMin.y);
        Fill.anchorMax = new Vector2(1 - minX, Fill.anchorMax.y);
    }
}
