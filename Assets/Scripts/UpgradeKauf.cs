﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeKauf : MonoBehaviour
{
    public Upgrade Upgrade;
    public UpgradeKeeper UpgradeKeeper;
    public ScoreKeeper ScoreKeeper;
    public TextMeshProUGUI ButtonText;
    public TextMeshProUGUI DescriptionText;

    public void Start()
    {
        ButtonText.text = Upgrade.Name;
    }

    public void Update()
    {
        GetComponent<Button>().interactable = ScoreKeeper.Score >= Upgrade.Cost;
    }

    public void Acquire()
    {
        UpgradeKeeper.Acquired.Add(Upgrade);
        ScoreKeeper.Score -= Upgrade.Cost;
    }

    public void Select()
    {
        DescriptionText.text = $"{Upgrade.Description}\n\nCost: {Upgrade.Cost}";
    }
}
