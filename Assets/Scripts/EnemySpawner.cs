﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float EnemyLine = 20.0f;
    public float EnemyLineWidth = 10.0f;
    public int MinEnemiesInWaveCount = 3;
    public int MaxEnemiesInWaveCount = 10;
    public int MaxEnemies = 30;
    public int WavePeriod = 5;
    public GameObject[] EnemyPrefabs;
    public GameObject Player;
    public GameObject Boss;
    public float BossTime;
    public Vector3 InitialBossPosition = new Vector3(0, 0, 25);
    public ScoreKeeper ScoreKeeper;

    public void Start()
    {
        StartCoroutine(SpawnWaves());
        StartCoroutine(SpawnBoss());
    }

    private IEnumerator SpawnWaves()
    {
        while(!EndCondition.IsGameOver && (Boss == null || BeatMaster.Instance.Time < BossTime))
        {
            var enemyCount = Random.Range(MinEnemiesInWaveCount, MaxEnemiesInWaveCount);
            for (int i = 0; i < enemyCount; ++i)
            {
                var enemyPrefab = EnemyPrefabs[Random.Range(0, EnemyPrefabs.Length)];
                var x = Random.Range(-EnemyLineWidth, EnemyLineWidth);
                var z = EnemyLine + i;
                var enemy = Instantiate(enemyPrefab, new Vector3(x, 0, z), Quaternion.AngleAxis(180, Vector3.up));
                var dumbo = enemy.GetComponent<DumbEnemy>();
                dumbo.Player = Player;
                dumbo.ScoreKeeper = ScoreKeeper;
            }
            yield return new WaitForSecondsRealtime(WavePeriod);
        }
    }

    private IEnumerator SpawnBoss()
    {
        if (Boss != null)
        {
            yield return new WaitForSecondsRealtime(BossTime - BeatMaster.Instance.Time);    
            var boss = Instantiate(Boss);
            boss.transform.position = InitialBossPosition;
            boss.GetComponent<Boss>().Initialize(Player, ScoreKeeper);
        }
    }
}
