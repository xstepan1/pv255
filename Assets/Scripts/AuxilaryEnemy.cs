﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxilaryEnemy : MonoBehaviour, IKillable
{
    public GameObject Player;
    public int StartHealth = 200;
    public ScoreKeeper ScoreKeeper;
    public AuxilaryChain Chain;

    private new Rigidbody rigidbody;
    private CenterBar bar;

    public int Health { get; set; }

    private Quaternion originalRotation;

    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        bar = GetComponentInChildren<CenterBar>();
        Health = StartHealth;
        originalRotation = rigidbody.rotation;
    }

    public void Update()
    {
        if (Health <= 0)
        {
            if (!EndCondition.IsGameOver)
            {
                ScoreKeeper.Score++;
            }
            gameObject.Explode();
            Chain.Explode();
            Destroy(gameObject);
        }
        bar.SetValue((float)Health / StartHealth);
    }

    public void FixedUpdate()
    {
        var direction = EndCondition.IsGameOver ? Vector3.back : Player.transform.position - transform.position;
        rigidbody.rotation = Quaternion.FromToRotation(Vector3.back, direction);
    }
}
