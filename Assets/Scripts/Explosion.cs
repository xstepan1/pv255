﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public AudioClip Sound;
    public float SoundVolume = 1;
    public GameObject ExplosionPrefab;
    public float Scale = 1;

    public void Explode()
    {
        var explosion = Instantiate(ExplosionPrefab, transform.position, transform.rotation);
        explosion.transform.localScale = Vector3.one * Scale;
        if (Sound is object && Sound != null)

        {
            BeatMaster.Instance.PlayOnce(Sound, SoundVolume);
        }
    }
}
