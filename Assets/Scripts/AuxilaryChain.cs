﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxilaryChain : MonoBehaviour
{
    public float Period = 1;

    public void Explode()
    {
        StartCoroutine(ExplodeCoroutine());
    }

    private IEnumerator ExplodeCoroutine()
    {
        while(gameObject.transform.childCount != 0)
        {
            var last = gameObject.transform.GetChild(gameObject.transform.childCount - 1).gameObject;
            last.Explode();
            Destroy(last);
            yield return new WaitForSecondsRealtime(Period);
        }
    }
}
