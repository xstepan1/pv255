﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(IKillable))]
public class LayeredCollision : MonoBehaviour
{
    public int OpponentLayer = -1;
    public int DefaultDamage = 50;

    private IKillable life;

    public void Start()
    {
        life = GetComponent<IKillable>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (life is null && other.gameObject.layer != OpponentLayer)
        {
            return;
        }

        var shot = other.GetComponent<MetaShot>();
        life.Health -= shot is object ? shot.SourceGun.ShotDamage : DefaultDamage;
    }
}
