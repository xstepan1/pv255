﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour, IKillable
{
    public Rect Bounds;
    public float Speed = 10;
    public int StartHealth = 100;
    public UpgradeKeeper UpgradeKeeper;

    private new Rigidbody rigidbody;
    private CenterBar bar;

    public int Health { get; set; }

    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Health = StartHealth;
        bar = GetComponentInChildren<CenterBar>();

        foreach (var upgrade in UpgradeKeeper.Acquired)
        {
            upgrade.Apply(gameObject);
        }
    }

    public void Update()
    {
        if (!EndCondition.IsGameOver)
        {
            bar.SetValue((float)Health / StartHealth);
        }
        else
        {
            bar.SetValue(0);
        }
    }

    public void FixedUpdate()
    {
        if (EndCondition.IsGameOver)
        {
            return;
        }

        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");

        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            var dir = new Vector2(
                (touch.position.x - Screen.width / 2) / 10 - rigidbody.position.x,
                (touch.position.y - Screen.height / 2) / 10 - rigidbody.position.z);
            dir.Normalize();
            horizontal = dir.x;
            vertical = dir.y;
        }

        rigidbody.velocity = new Vector3(horizontal, 0, vertical) * Speed;
        rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -2);
        rigidbody.position = new Vector3(
            Mathf.Clamp(rigidbody.position.x, Bounds.xMin, Bounds.xMax),
            rigidbody.position.y,
            Mathf.Clamp(rigidbody.position.z, Bounds.yMin, Bounds.yMax));
    }
}
