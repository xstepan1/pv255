﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeapotGun : MonoBehaviour
{
    public bool Automatic = true;
    public float Period = 1;
    public int ShotLayer;
    public float ShotSpeed = 10;
    public int ShotDamage = 10;
    public GameObject Shot;
    public AudioClip Sound;
    public float SoundVolume;
    public Vector3 Direction = Vector3.forward;

    public void Start()
    {
        StartCoroutine(AutoFire());
    }

    public void Fire()
    {
        var shot = Instantiate(Shot, transform.position, transform.rotation);
        shot.SetLayer(ShotLayer);
        BeatMaster.Instance.PlayOnce(Sound, SoundVolume);

        shot.GetComponent<MetaShot>().SourceGun = this;

        var shotRigidbody = shot.GetComponent<Rigidbody>();
        shotRigidbody.velocity = transform.rotation * Direction * ShotSpeed;
    }

    private IEnumerator AutoFire()
    {
        while (!EndCondition.IsGameOver)
        {
            Fire();
            yield return new WaitForSecondsRealtime(Period);
        }
    }
}
