﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndCondition : MonoBehaviour
{
    public const string Success = "You Did Not Die";
    public const string Failure = "You Died";
    public const string Surrender = "You Quit";

    public static bool IsGameOver = false;
    public static bool IsGameWon = false;

    public GameObject Player;
    public DarthFader GameOverOverlay;
    public TextMeshProUGUI Text;
    public float ReturnToMenuLength = 5;
    public float LoseAfterLength = -1;

    private IKillable player;

    public void Awake()
    {
        IsGameOver = false;
        IsGameWon = false;
        player = Player.GetComponent<IKillable>();
        if (LoseAfterLength >= 0)
        {
            StartCoroutine(LoseAfter());
        }
    }

    public void Update()
    {
        if (IsGameWon)
        {
            Win();
        }
        else if (player.Health <= 0 && !IsGameOver)
        {
            Lose();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Lose(Surrender);
        }
    }

    private void Win()
    {
        IsGameOver = true;
        IsGameWon = true;
        Text.text = Success;
        GameOverOverlay.FadeIn();
        StartCoroutine(ReturnToMenu());
    }

    private void Lose(string text = Failure)
    {
        Player.Explode();
        Destroy(Player);
        IsGameOver = true;
        IsGameWon = false;
        Text.text = text;
        GameOverOverlay.FadeIn();
        StartCoroutine(ReturnToMenu());
    }

    private IEnumerator ReturnToMenu()
    {
        yield return new WaitForSeconds(ReturnToMenuLength);
        SceneManager.LoadScene("Menu");
    }

    private IEnumerator LoseAfter()
    {
        yield return new WaitForSeconds(LoseAfterLength);
        Lose();
    }
}
