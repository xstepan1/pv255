﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPrefsReset : MonoBehaviour
{
    public ScoreKeeper ScoreKeeper;
    public UpgradeKeeper UpgradeKeeper;

    public void Update()
    {
        if (Debug.isDebugBuild && Input.GetKeyDown(KeyCode.R))
        {
            ScoreKeeper.Score = 0;
            PlayerPrefs.DeleteKey(ScoreKeeper.ScorePrefsKey);

            UpgradeKeeper.Acquired.Clear();
            PlayerPrefs.DeleteKey(UpgradeKeeper.UpgradesPrefsKey);

            PlayerPrefs.Save();
        }
    }
}
