﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNudge : MonoBehaviour
{
    public Rigidbody Target;
    public float Scale = 1;

    public void NudgeRandomly()
    {
        Target.AddTorque(new Vector3(Random.Range(-Scale, Scale), Random.Range(-Scale, Scale), Random.Range(-Scale, Scale)));
    }
}
