﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Upgrade : ScriptableObject
{
    public string Name;
    public string Key;
    public string Description;
    public float GunPeriodFactor = 1;
    public float ShotSpeedFactor = 1;
    public float ShotDamageFactor = 1;
    public float ShipSpeedFactor = 1;
    public float ShipHealthFactor = 1;
    public int Cost = 10;

    public void Apply(GameObject player)
    {
        var gun = player.GetComponent<TeapotGun>();
        gun.Period *= GunPeriodFactor;
        gun.ShotSpeed *= ShotSpeedFactor;
        gun.ShotDamage = (int)Mathf.Ceil(gun.ShotDamage * ShotDamageFactor);

        var spaceship = player.GetComponent<Spaceship>();
        spaceship.Speed *= ShipSpeedFactor;
        spaceship.Health = (int)Mathf.Ceil(spaceship.Health * ShipHealthFactor);
    }
}
