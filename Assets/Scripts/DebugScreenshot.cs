﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class DebugScreenshot : MonoBehaviour
{
    public void Update()
    {
        if (Debug.isDebugBuild && Input.GetKeyDown(KeyCode.P))
        {
            var dir = Path.Combine(Path.GetTempPath(), "tearian");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var filename = DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss");
            var path = Path.Combine(dir, $"{filename}.png");
            ScreenCapture.CaptureScreenshot(path);
            Debug.Log($"Screenshot saved to '{path}'.");
        }
    }
}
