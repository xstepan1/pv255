﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class GameObjectExtensions
{
    public static void Explode(this GameObject self)
    {
        var explosion = self.GetComponent<Explosion>();
        if (explosion is object)
        {
            explosion.Explode();
        }
    }

    public static void SetLayer(this GameObject self, int layer)
    {
        self.layer = layer;
        foreach(var child in self.transform.GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = layer;
        }
    }
}
