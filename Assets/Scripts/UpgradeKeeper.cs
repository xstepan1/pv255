﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UpgradeKeeper : MonoBehaviour
{
    public const string UpgradesPrefsKey = "Upgrades";

    public Upgrade[] Assortment;

    public List<Upgrade> Acquired { get; } = new List<Upgrade>();

    public void Awake()
    {
        var acquiredString = PlayerPrefs.GetString(UpgradesPrefsKey);
        var acquiredKeys = acquiredString.Split(';');
        foreach(var key in acquiredKeys)
        {
            var upgrade = Assortment.FirstOrDefault(u => u.Key == key);
            if (upgrade is null)
            {
                Debug.Log($"There is no upgrade with the '{key}' key.");
                continue;
            }

            Acquired.Add(upgrade);
        }
    }

    public void OnDestroy()
    {
        var acquiredString = string.Join(";", Acquired.Select(u => u.Key));
        PlayerPrefs.SetString(UpgradesPrefsKey, acquiredString);
        PlayerPrefs.Save();
    }
}
