﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSoundEnforcer : MonoBehaviour
{
    public AudioClip Sound;

    public void Awake()
    {
        // I'm not gonna add the event to every single button, so this is my solution
        var button = GetComponent<Button>();
        button.onClick.AddListener(() => BeatMaster.Instance.PlayOnce(Sound));
    }
}
