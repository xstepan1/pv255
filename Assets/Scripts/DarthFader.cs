﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarthFader : MonoBehaviour
{
    public CanvasGroup Canvas;
    public float FadeInLength = 1;
    public float FadeOutLength = 1;

    public void FadeIn()
    {
        StartCoroutine(FadeInCoroutine());
    }

    public void FadeOut()
    {
        StartCoroutine(FadeOutCoroutine());
    }

    private IEnumerator FadeInCoroutine()
    {
        float start = Time.time;
        while (Canvas.alpha < 1)
        {
            Canvas.alpha += (Time.time - start) / FadeInLength;
            yield return new WaitForEndOfFrame();
        }
        Canvas.interactable = true;
        Canvas.blocksRaycasts = true;
    }

    private IEnumerator FadeOutCoroutine()
    {
        Canvas.interactable = false;
        Canvas.blocksRaycasts = false;
        float start = Time.time;
        while (Canvas.alpha > 0)
        {
            Canvas.alpha -= (Time.time - start) / FadeOutLength;
            yield return new WaitForEndOfFrame();
        }
    }
}
