﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpgradeVitrine : MonoBehaviour, IPointerEnterHandler, ISelectHandler
{
    public GameObject ButtonTemplate;
    public TextMeshProUGUI DescriptionText;
    public Transform ButtonsWrapper;
    public UpgradeKeeper UpgradeKeeper;
    public ScoreKeeper ScoreKeeper;

    public void Start()
    {
        DescriptionText.text = string.Empty;
        foreach (var upgrade in UpgradeKeeper.Assortment)
        {
            var button = Instantiate(ButtonTemplate, ButtonsWrapper);
            var kauf = button.GetComponent<UpgradeKauf>();
            kauf.Upgrade = upgrade;
            kauf.DescriptionText = DescriptionText;
            kauf.UpgradeKeeper = UpgradeKeeper;
            kauf.ScoreKeeper = ScoreKeeper;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var kauf = eventData.pointerEnter.GetComponentInParent<UpgradeKauf>();
        if (kauf is object)
        {
            kauf.Select();
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        eventData.selectedObject.GetComponent<UpgradeKauf>().Select();
    }
}
