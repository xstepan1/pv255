#!/bin/bash

if [ ! -d ./Builds ]; then
    mkdir ./Builds
fi

~/Unity/Hub/Editor/2019.4.13f1/Editor/Unity \
    -logFile /dev/stdout \
    -batchmode \
    -projectPath $(pwd) \
    -quit \
    -buildTarget WebGL \
    -customBuildPath $(pwd)/Builds \
    -executeMethod Build.Perform
